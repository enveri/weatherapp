# Weatherapp task by eficode  
Task can be found from [here](https://github.com/Eficode/weatherapp/).  

## To run the project  
$ docker-compose up  
The project is running at [localhost:8000](http://localhost:8000).  

## Tasks done:

* Mandatory tasks done.  

* The application now reports the forecast a few hours from now.  

* Developement files are now shared for the container by using volumes and webpack server is also using HMR.  

* Critical eslint errors are now fixed.  
